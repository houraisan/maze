#include "generate.h"


static void insertion_sort(int ary[][2], const int sz)
{
    // insertion sort for int-pair.
    for (int i = 1; i < sz; ++i)
    {
        if (ary[i][1] > ary[i - 1][1]) {

            int tmp[2] = {ary[i][0], ary[i][1]};

            int j = i - 1;
            for (; j >= 0 && ary[j][1] > tmp[1]; --j)
            {
                ary[j][0] = ary[j + 1][0];
                ary[j][1] = ary[j + 1][1];
            }

            ary[j][0] = tmp[0];
            ary[j][1] = tmp[1];
        }
    }
}


static int maze_initialize(Maze_state maze)
{
    // raw initialize
    for (int i = 0; i < MAZE_ROW; ++i)
    {
        for (int j = 0; j < MAZE_COLUMN; ++j)
        {
            if ((i & 1) && (j & 1)) {

                // odd points
                maze[i][j] = initial;
            } else {

                // other points
                // just set to block
                maze[i][j] = block;
            }
        }
    }

    return 0;
}



// generate a maze using randomized dfs
static int next_step(Maze_state maze, const int i, const int j)
{
    assert(i & 1 && j & 1);
    assert(IN_RANGE(i, j));

    // storage neighbours
    int cnt = 0;
    int node[4][2];

    // iterate neighbours
    for (int it = 0; it < 4; ++it)
    {
        int x = eye[it][0] * 2 + i;
        int y = eye[it][1] * 2 + j;

        if (IN_RANGE(x, y)) {

            // add to list
            node[cnt][0] = x;
            node[cnt][1] = y;

            ++cnt;
        }
    }

    // give all valid neighbours a random weight
    int map[4][2];
    for (int it = 0; it < 4; ++it)
    {
        map[it][0] = it;
        map[it][1] = rand();
    }

    // sort by weight
    insertion_sort(map, cnt);

    // do recursion in randomized order
    for (int it = 0; it < cnt; ++it)
    {
        const int index = map[it][0];

        // extract position
        int x = node[index][0];
        int y = node[index][1];

        // if next node is not visited, go to that node
        // color `gray` means not visited
        if (maze[x][y] == initial) {

            // middle of two cells
            int mid_x = x + (i - x) / 2;
            int mid_y = y + (j - y) / 2;

            // set visited
            maze[mid_x][mid_y] = empty;
            maze[x][y] = empty;

            // recur
            next_step(maze, x, y);
        }
    }

    return 0;
}


int maze_generate(Maze_state maze)
{
    int sig; // terminate signal

    sig = maze_initialize(maze);
    if (sig) return 1;

    // set pseudo random number seed
    srand((unsigned)time(NULL) ^ rand());

    // generate random maze by recursion
    sig = next_step(maze, 1, 1);
    if (sig) return 1;

    // set beginning and ending points

    // a is row number of begin point,
    // b is row number of end point
    int a, b;
    // column number of begin point is 0,
    // column number of end point is MAZE_COLUMN - 1.
    {
        // initialize a and b

        // storage all possible row numbers
        int * as = malloc(sizeof(int) * MAZE_ROW);
        int * bs = malloc(sizeof(int) * MAZE_ROW);

        int ai = 0;
        int bi = 0;

        // iterate all side cells
        for (int i = 0; i < MAZE_ROW; ++i)
        {
            if (maze[i][1] == empty) {
                as[ai++] = i;
            }
            if (maze[i][MAZE_COLUMN - 2] == empty) {
                bs[bi++] = i;
            }
        }

        if (!ai || !bi) {
            return 1; // error occurred
        }

        // select random beginning and ending from lists
        a = as[rand() % ai];
        b = bs[rand() % bi];

        // free allocated memory
        free(as);
        free(bs);
    }

    maze[a][0] = beginning;
    maze[b][MAZE_COLUMN - 1] = ending;

    return 0;
}


int maze_set_current_begin(Maze_state maze, int position[2])
{
    for (int i = 0; i < MAZE_ROW; ++i)
    {
        if (maze[i][0] == beginning) {

            // set and return
            position[0] = i;
            position[1] = 0;

            return 0;
        }
    }

    // uninitialized. report error.
    return 1;
}