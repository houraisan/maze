#include "maze.h"

// sub modules
#include "generate.h"
#include "render.h"


void maze_destroy(Maze maze)
{
    /* free allocated memory of maze
     *
     * it should be manually called every time you no longer need a maze object.
     */
    if (maze != NULL) {
        free(maze);
    }
}


Maze maze_new()
{
    /* generate a new maze
     *
     * create a brand new maze from heap memory. thus you should always
     * remember to call `maze_destroy` when you don't need this object.
     *
     * should only call once if only one maze is needed at the same time.
     * if you're to update the maze, don't just call destroy and this function;
     * instead, calling the `maze_regen` function.
     */

    // allocate memory from heap
    Maze maze = malloc(sizeof(struct maze_container));

    int sig = 0;

    // generate random maze
    sig += maze_generate(maze->state);

    // setup start position
    maze_set_current_begin(maze->state, maze->current);

    // setup color blocks
    sig += maze_render_color(maze->state, maze->color);

    // assemble graphical widget
    sig += maze_render_widget(maze->color, &(maze->grid));

    assert(!sig);
    return maze;
}


static int dfs(Maze_state maze, int i, int j)
{
    /* searching way using dfs
     *
     * this is recursive part of dfs, set result by flagging
     * correct way.
     *
     * if doesn't find a solution (way to ending position),
     * return 1; else return 0 as a normal execution.
     */

    // mark current position as traveled, thus it won't
    // course a backward searching
    maze[i][j] = traveled;

    // iterate over neighbours
    for (int it = 0; it < 4; ++it)
    {
        // position of this neighbour
        int x = eye[it][0] + i;
        int y = eye[it][1] + j;

        // make sure it is inner the maze body
        if (IN_RANGE(x, y)) {

            // judging by neighbour's state
            Flag st = maze[x][y];

            // basement or swapped up and recursion
            if (st == ending || (st == empty && !dfs(maze, x, y))) {

                // trace back
                maze[i][j] = result;

                /* return with success */
                return 0;
            }

            // otherwise, search on next neighbour
        }
    }

    /* bad search. return errno. */
    return 1;
}


static int update_solution(Maze ptr, int i, int j)
{
    /* update maze by setup flags
     *
     * it's also dfs. the reason of separating this one with
     * flag-setting fallible dfs is for better readability.
     */

    // set current position to flag traveled in order
    // to prevent from backward tracing.
    (ptr->state)[i][j] = traveled;

    // iterate over all neighbours
    for (int it = 0; it < 4; ++it)
    {
        // position of neighbour
        int x = eye[it][0] + i;
        int y = eye[it][1] + j;

        // make sure this position is inner the maze body
        if (IN_RANGE(x, y)) {
            // judging by neighbour's state
            Flag st = (ptr->state)[x][y];

            // basement of recursion. return.
            if (st == ending) {
                maze_render_update(ptr, x, y, red_color);
                return 0;
            }
            // recursively update current cell
            if (st == result) {
                update_solution(ptr, x, y);
                maze_render_update(ptr, x, y, red_color);

                return 0;
            }

            // otherwise, judging next one.
        }
    }

    return 1;
}


int maze_show_solution(struct maze_container * maze)
{
    /* show solution of maze
     *
     * it should only be called once for one maze.
     */

    // position of beginning point
    const int x = (maze->current)[0];
    const int y = (maze->current)[1];

    int sig = 0;

    sig += dfs(maze->state, x, y);

    sig += update_solution(maze, x, y);

    maze->state[x][y] = ending;
    return sig;
}


int maze_check_end(Maze m)
{
    return m->state[m->current[0]][m->current[1]] != ending;
}


static int maze_check_move(Maze_state maze, const int *move)
{
    if (!IN_RANGE(move[0], move[1])) {
        return 1;
    }

    Flag f = maze[move[0]][move[1]];

    if (f == block || f == initial) {
        return 1;
    }

    return 0;
}



int maze_move_try_direct(Maze maze, int i)
{
    int move[2];

    move[0] = eye[i][0] + maze->current[0];
    move[1] = eye[i][1] + maze->current[1];

    if (maze_check_move(maze->state, move)) {
        return 1;
    }

    maze_render_update(maze, move[0], move[1], red_color);
    maze_render_update(maze, maze->current[0], maze->current[1], black_color);

    maze->current[0] = move[0];
    maze->current[1] = move[1];

    return 0;
}


void maze_refresh(Maze maze)
{
    /* refresh maze
     *
     * refresh maze state by recalling maze
     * generating and rendering functions.
     */
    int sig = 0;

    maze_generate(maze->state);
    sig += maze_set_current_begin(maze->state, maze->current);
    sig += maze_render_color(maze->state, maze->color);

    assert(!sig);
    maze_render_refresh_all(maze);
}