#ifndef MAZE_GENERATE_H
#define MAZE_GENERATE_H


#include <stdlib.h>
#include <assert.h>
#include <stdio.h>


#include <gtk/gtk.h>


#include "maze.h"


int maze_generate(Maze_state);


int maze_set_current_begin(Maze_state, int [2]);


#endif //MAZE_GENERATE_H