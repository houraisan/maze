#include "render.h"


int maze_render_color(Maze_state maze, Maze_color color)
{
    for (int i = 0; i < MAZE_ROW; ++i)
    {
        for (int j = 0; j < MAZE_COLUMN; ++j)
        {
            const char * ptr;

            switch (maze[i][j])
            {
                case block:
                case initial:
                    ptr = white_color;
                    break;

                case empty:
                    ptr = black_color;
                    break;

                case beginning:
                    ptr = red_color;
                    break;

                case ending:
                    ptr = blue_color;
                    break;

                default:
                    // return errno
                    return 1;
            }

            gdk_rgba_parse(&color[i][j], ptr);
        }
    }

    return 0;
}


static gboolean cell_callback(GtkWidget * cell, cairo_t * cr, gpointer color)
{
    GtkStyleContext * context = gtk_widget_get_style_context(cell);

    gtk_render_background(context, cr, 0, 0, CELL_WIDTH, CELL_HEIGHT);

    cairo_rectangle(cr, 0, 0, CELL_WIDTH, CELL_HEIGHT);
    gdk_cairo_set_source_rgba(cr, color);

    cairo_fill(cr);
    return FALSE;
}


int maze_render_widget(Maze_color color, GtkWidget ** grid)
{
    *grid = gtk_grid_new();

    for (int i = 0; i < MAZE_ROW; ++i)
    {
        for (int j = 0; j < MAZE_COLUMN; ++j)
        {
            GtkWidget * cell = gtk_drawing_area_new();

            gtk_widget_set_size_request(cell, CELL_WIDTH, CELL_HEIGHT);
            g_signal_connect(G_OBJECT(cell), "draw", G_CALLBACK(cell_callback), &color[i][j]);

            gtk_grid_attach(GTK_GRID(*grid), cell, j, i, 1, 1);
        }
    }
}


static void render_update_cell(GtkWidget * grid, int i, int j)
{
    GtkWidget * cell = gtk_grid_get_child_at(GTK_GRID(grid), j, i);
    gtk_widget_queue_draw(cell);
}


void maze_render_update(Maze maze, int i, int j, const char *color_name)
{
    gdk_rgba_parse(&maze->color[i][j], color_name);
    render_update_cell(maze->grid, i, j);
}


int maze_render_refresh_all(Maze maze)
{
    for (int i = 0; i < MAZE_ROW; ++i)
    {
        for (int j = 0; j < MAZE_COLUMN; ++j)
        {
            render_update_cell(maze->grid, i, j);
        }
    }
}