#ifndef MAZE_MAZE_H
#define MAZE_MAZE_H


/* maze ADT */


// gtk library
#include <gtk/gtk.h>


// standard library
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


/// individual functions


#define MAZE_WIDTH 20
#define MAZE_HEIGHT 30


#define CELL_WIDTH 10
#define CELL_HEIGHT 10


#define MAZE_ROW (MAZE_WIDTH | 1)
#define MAZE_COLUMN (MAZE_HEIGHT | 1)


enum cell_flag {

    // searching flag
    traveled,
    result,

    // representing maze structure
    block,
    empty,

    // generating flag
    initial,

    // beginning and ending flag
    beginning,
    ending
};


typedef enum cell_flag Flag;


struct maze_container {

    // flag of cell. used in maze generation and solving
    Flag state[MAZE_ROW][MAZE_COLUMN];

    // color of every widget. used in redrawing
    GdkRGBA color[MAZE_ROW][MAZE_COLUMN];

    // grid object
    GtkWidget * grid;

    // current state.
    // point to current position
    int current[2];
};


// Maze ADT
typedef struct maze_container * Maze;


// maze statement representation
typedef Flag Maze_state[MAZE_ROW][MAZE_COLUMN];


// maze color grid
typedef GdkRGBA Maze_color[MAZE_ROW][MAZE_COLUMN];


// creating and deleting maze
Maze maze_new();
void maze_destroy(Maze maze);


// generate maze on top of an existing maze
void maze_refresh(Maze maze);


// show solution
int maze_show_solution(Maze);


// manual solving
int maze_check_end(Maze);
int maze_move_try_direct(Maze, int);


/// iteration of maze


// condition
#define IN_RANGE(x, y) (x > 0 && x < MAZE_ROW && y > 0 && y < MAZE_COLUMN)


// iteration
const static int eye[4][2] = {
        {-1, 0},
        {0, -1},

        {1, 0},
        {0, 1}
};


#endif //MAZE_MAZE_H