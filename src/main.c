#include "main.h"


static void destroy_callback(GtkWidget * widget, gpointer data)
{
    Maze maze = data;

    maze_destroy(maze);
}


static void solve_callback(GtkWidget * widget, gpointer data)
{
    Maze maze = data;

    if (maze_check_end(maze)) {
        maze_show_solution(maze);
    }
}


static void restart_callback(GtkWidget * widget, gpointer data)
{
    Maze maze = data;

    maze_refresh(maze);
}


static gboolean manual_callback(GtkWidget * widget, GdkEventKey * event, gpointer data)
{
    Maze maze = data;

#ifndef NDEBUG
    printf("keyboard: %c key pressed\n", event->keyval);
#endif

    if (maze_check_end(maze)) {

        int signum;
        switch (event->keyval)
        {
            case GDK_KEY_w:
            case GDK_KEY_W:
            case 65362:
                signum = 0;
                break;

            case GDK_KEY_a:
            case GDK_KEY_A:
            case 65361:
                signum = 1;
                break;

            case GDK_KEY_s:
            case GDK_KEY_S:
            case 65364:
                signum = 2;
                break;

            case GDK_KEY_d:
            case GDK_KEY_D:
            case 65363:
                signum = 3;
                break;

            default:
                return FALSE;
        }

        if (maze_move_try_direct(maze, signum)) {
            gdk_display_beep(gdk_display_get_default());
        }

        if (!maze_check_end(maze)) {
            const GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
            const GtkMessageType types = GTK_MESSAGE_INFO;
            const GtkButtonsType buttons = GTK_BUTTONS_CLOSE;

            GtkWidget * dialog = gtk_message_dialog_new(GTK_WINDOW(widget), flags, types, buttons, "Success!");
            gtk_dialog_run(GTK_DIALOG(dialog));
            gtk_widget_destroy(dialog);
        }
    }

    return FALSE;
}


// gtk application callback function.
static void app_callback(GtkApplication *app, gpointer cfg)
{
    /// creating widgets

    // create window
    GtkWidget * window = gtk_application_window_new(app);
    // customizing window
    gtk_window_set_title(GTK_WINDOW(window), "maze");
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);

    // create title bar
    GtkWidget * header = gtk_header_bar_new();
    // customizing title bar
    gtk_header_bar_set_title(GTK_HEADER_BAR(header), "");
    gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(header), TRUE);
    gtk_header_bar_set_has_subtitle(GTK_HEADER_BAR(header), FALSE);

    // create maze
    Maze maze = maze_new();
    g_signal_connect(window, "destroy", G_CALLBACK(destroy_callback), maze);

    // button container grid
    GtkWidget * menu_grid = gtk_grid_new();

    // button connected to maze_solution
    GtkWidget * solve_button = gtk_button_new_with_label("Solution");
    g_signal_connect(solve_button, "clicked", G_CALLBACK(solve_callback), maze);
    // attach to grid
    gtk_grid_attach(GTK_GRID(menu_grid), solve_button, 0, 0, 1, 1);

    // button connected to maze_restart
    GtkWidget * restart_button = gtk_button_new_with_label("Restart");
    g_signal_connect(restart_button, "clicked", G_CALLBACK(restart_callback), maze);
    // attach to grid
    gtk_grid_attach(GTK_GRID(menu_grid), restart_button, 1, 0, 1, 1);


    /// linking widgets

    // add buttons to title bar
    gtk_container_add(GTK_CONTAINER(header), menu_grid);
    // add title bar to window
    gtk_window_set_titlebar(GTK_WINDOW(window), header);

    // linking to maze
    gtk_container_add(GTK_CONTAINER(window), maze->grid);

    /// keyboard press
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK(manual_callback), maze);

    // show window
    gtk_widget_show_all(window);
}


int main(int argc, char * argv[])
{
    // create a gtk window application associate with `applicative` function
    GtkApplication * app;

    app = gtk_application_new("org.gtk.maze", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(app_callback), NULL);

    // execute application
    int return_sig = g_application_run(G_APPLICATION(app), argc, argv);

    /// release resources

    // release gui application
    g_object_unref(app);


    return return_sig;
}