#ifndef MAZE_RENDER_H
#define MAZE_RENDER_H


#include <gtk/gtk.h>


#include "maze.h"





// color names

static const char * red_color = "Red";
static const char * blue_color = "DeepSkyBlue";
static const char * black_color = "Gray";
static const char * white_color = "FloralWhite";


int maze_render_color(Maze_state, Maze_color);


int maze_render_widget(Maze_color, GtkWidget **);


void maze_render_update(Maze maze, int i, int j, const char *color_name);


int maze_render_refresh_all(Maze);


#endif //MAZE_RENDER_H