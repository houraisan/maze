# maze game

simple practical maze game using gtk+.

## how to run

- gtk3 library is needed. if you don't have gtk3 on your
computer, check 
[gtk official website](https://www.gtk.org/download/index.php)
for installation.

- also, you will need cmake to execute the build process.
the minimum requirement of cmake is 3.11 .

### Linux

first, run cmake on project's root directory.

``` bash
$ cmake .
```

then run make. it will generate the target file.

``` bash
$ make
```

now you have the program named `maze`. just run it.

``` bash
$ ./maze
```

### Windows

it could be difficult installing gtk3 on Windows, thus
I recommend just open a virtual machine running Linux.

An alternate solution is
[Windows subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/faq)
known as WSL, though gui is not officially supported by that.